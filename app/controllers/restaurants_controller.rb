class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: [:show, :edit, :update, :destroy]

  # GET /restaurants
  # GET /restaurants.json
  def index
    #@restaurants = Restaurant.all
    @restaurants = Restaurant.search_for(params[:search]).paginate(page: params[:page], per_page: 5).order('id DESC')
  end

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show
    @comment = @restaurant.comments.build()
  end

  # GET /restaurants/new
  def new
    @restaurant = Restaurant.new
    authorize! :create, @restaurant
  end

  # GET /restaurants/1/edit
  def edit
    authorize! :update, @restaurant
    redirect_to restaurants_path if @restaurant.user_id != current_user.id && current_user.role != "admin"
  end
  # POST /restaurants
  # POST /restaurants.json
  def create
    @restaurant = Restaurant.new(restaurant_params)
    authorize! :create, @restaurant
    respond_to do |format|
      if @restaurant.save
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully created.' }
        format.json { render action: 'show', status: :created, location: @restaurant }
      else
        format.html { render action: 'new' }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /restaurants/1
  # PATCH/PUT /restaurants/1.json
  def update
    authorize! :update, @restaurant
    redirect_to restaurants_path if @restaurant.user_id != current_user.id && current_user.role != "admin"

    respond_to do |format|
      if @restaurant.update(restaurant_params)
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /restaurants/1
  # DELETE /restaurants/1.json
  def destroy
    authorize! :destroy, @restaurant
    @restaurant.destroy
    respond_to do |format|
      format.html { redirect_to manage_review_restaurants_path,notice: 'Restaurant was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def rating_average
    score = params[:star1].to_i+params[:star2].to_i+params[:star3].to_i+params[:star4].to_i
    score = score / 4.0
    res = Restaurant.find(params[:restaurant_id])
    click_count_old = res.count.to_i
    click_count_new = click_count_old + 1
    average = ( ((click_count_old * res.total.to_f) + score ) / click_count_new)
    if res.update(:total => average,:count => click_count_new)
      Rating.create(total: score,restaurant_id: params[:restaurant_id],user_id: params[:user_id])
      respond_to do |format|
        format.html { redirect_to restaurant_path(params[:restaurant_id]), notice: 'Thank you for vote this restaurant.' }
      end
    else
      respond_to do |format|
        format.html { redirect_to restaurant_path(params[:restaurant_id]), notice: 'Can not vote.' }
      end
    end
  end

  def manage_review
     # @restaurants = Restaurant.paginate(page: params[:page], per_page: 30)
    if current_user.role == "admin"
      @restaurants = Restaurant.all
    else
      @restaurants = Restaurant.where("user_id = ?",current_user.id)
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def restaurant_params
      params.require(:restaurant).permit(:name, :address, :tel, :email, :website, :description, :price_per_person, :office_hours, :locate_id, :user_id, :restaurant_category_id,:total ,:star1 ,:star2 ,:star3 ,:star4,:restaurant_id ,:images_attributes => ['id','image','description','_destroy'],:menus_attributes => ['id','name','_destroy'])
    end

end
