class HomepagesController < ApplicationController
	layout "homepage"
  def index
  end

  def about_us
  end

  def topten
  	@restaurants = Restaurant.where("total != 0.0").order("total DESC").limit(10)
  end
  
end
