json.array!(@locates) do |locate|
  json.extract! locate, :district
  json.url locate_url(locate, format: :json)
end
