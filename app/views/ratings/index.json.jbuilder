json.array!(@ratings) do |rating|
  json.extract! rating, :total, :user_id, :restaurant_id
  json.url rating_url(rating, format: :json)
end
