json.array!(@images) do |image|
  json.extract! image, :image, :description, :restaurant_id
  json.url image_url(image, format: :json)
end
