json.array!(@restaurants) do |restaurant|
  json.extract! restaurant, :name, :address, :tel, :email, :website, :description, :price_per_person, :office_hours, :locate_id, :user_id, :restaurant_category_id
  json.url restaurant_url(restaurant, format: :json)
end
