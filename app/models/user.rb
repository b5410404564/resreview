class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  ROLES = %w[admin member]

  mount_uploader :avartar, ImageUploader

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, 
         :authentication_keys => [:user_name]

  validates_uniqueness_of :user_name,:email
  validates :phone, phone: true

  has_many :restaurants ,dependent: :destroy       
  has_many :comments,dependent: :destroy
  has_many :ratings,dependent: :destroy

end
