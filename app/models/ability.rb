class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
      #user ||= User.new # guest user (not logged in)
      if user
        if user.role == "admin"
          can :manage, :all
          can :assign_roles
        elsif user.role == "member"

          can [:show,:new,:create,:edit,:update], User
          #cannot [:index,:destroy], User

          can :manage, [Restaurant,Rating,Image,Menu,Comment]
          #cannot :manage ,[RestaurantCategory,Locate]

        end
      else
        #can [:index,:show], Restaurant -> not nessary because index and show action note authorize
        cannot [:create],Restaurant
      end

    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
