class Restaurant < ActiveRecord::Base
	
	belongs_to :user
	belongs_to :locate
	belongs_to :restaurant_category

	has_many :ratings,dependent: :destroy
	has_many :comments,dependent: :destroy
	has_many :menus,dependent: :destroy
	has_many :images,dependent: :destroy


	validates_format_of :email,:multiline => true, :with => /^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i
	validates :name, presence: true,length: { maximum: 30 }
	validates :address, presence: true
	validates :description, presence: true
	validates :locate, presence: true
	validates :restaurant_category, presence: true
	validates :tel, phone: { possible: true, allow_blank: true } #use gem phonelib for validate


	accepts_nested_attributes_for :images, :reject_if => lambda { |a| a[:image].blank? }, :allow_destroy => true
	accepts_nested_attributes_for :menus, :reject_if => lambda { |b| b[:name].blank? }, :allow_destroy => true

	scoped_search :on => [:name, :locate_id ,:address ,:tel ,:website ,:email]
	scoped_search :in => :locate, :on => :district
	scoped_search :in => :restaurant_category, :on => :name
	scoped_search :in => :user, :on => :user_name



end
