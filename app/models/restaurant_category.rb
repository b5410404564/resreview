class RestaurantCategory < ActiveRecord::Base
	has_many :restaurants, dependent: :destroy
end
