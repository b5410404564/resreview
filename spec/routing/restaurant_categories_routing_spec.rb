require "spec_helper"

describe RestaurantCategoriesController do
  describe "routing" do

    it "routes to #index" do
      get("/restaurant_categories").should route_to("restaurant_categories#index")
    end

    it "routes to #new" do
      get("/restaurant_categories/new").should route_to("restaurant_categories#new")
    end

    it "routes to #show" do
      get("/restaurant_categories/1").should route_to("restaurant_categories#show", :id => "1")
    end

    it "routes to #edit" do
      get("/restaurant_categories/1/edit").should route_to("restaurant_categories#edit", :id => "1")
    end

    it "routes to #create" do
      post("/restaurant_categories").should route_to("restaurant_categories#create")
    end

    it "routes to #update" do
      put("/restaurant_categories/1").should route_to("restaurant_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/restaurant_categories/1").should route_to("restaurant_categories#destroy", :id => "1")
    end

  end
end
