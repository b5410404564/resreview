require "spec_helper"

describe LocatesController do
  describe "routing" do

    it "routes to #index" do
      get("/locates").should route_to("locates#index")
    end

    it "routes to #new" do
      get("/locates/new").should route_to("locates#new")
    end

    it "routes to #show" do
      get("/locates/1").should route_to("locates#show", :id => "1")
    end

    it "routes to #edit" do
      get("/locates/1/edit").should route_to("locates#edit", :id => "1")
    end

    it "routes to #create" do
      post("/locates").should route_to("locates#create")
    end

    it "routes to #update" do
      put("/locates/1").should route_to("locates#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/locates/1").should route_to("locates#destroy", :id => "1")
    end

  end
end
