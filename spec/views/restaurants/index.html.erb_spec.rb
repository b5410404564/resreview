require 'spec_helper'

describe "restaurants/index" do
  before(:each) do
    assign(:restaurants, [
      stub_model(Restaurant,
        :description => "Description",
        :price_per_person => "Price Per Person",
        :address => "Address",
        :office_hours => "Office Hours",
        :tel => "Tel",
        :email => "Email",
        :website => "Website",
        :reccomment_menu => "Reccomment Menu",
        :map_img => "Map Img",
        :store_img => "Store Img",
        :restaurant_id => 1
      ),
      stub_model(Restaurant,
        :description => "Description",
        :price_per_person => "Price Per Person",
        :address => "Address",
        :office_hours => "Office Hours",
        :tel => "Tel",
        :email => "Email",
        :website => "Website",
        :reccomment_menu => "Reccomment Menu",
        :map_img => "Map Img",
        :store_img => "Store Img",
        :restaurant_id => 1
      )
    ])
  end

  it "renders a list of restaurants" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Price Per Person".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => "Office Hours".to_s, :count => 2
    assert_select "tr>td", :text => "Tel".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Website".to_s, :count => 2
    assert_select "tr>td", :text => "Reccomment Menu".to_s, :count => 2
    assert_select "tr>td", :text => "Map Img".to_s, :count => 2
    assert_select "tr>td", :text => "Store Img".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
