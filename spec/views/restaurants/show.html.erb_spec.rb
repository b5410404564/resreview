require 'spec_helper'

describe "restaurants/show" do
  before(:each) do
    @restaurant = assign(:restaurant, stub_model(Restaurant,
      :description => "Description",
      :price_per_person => "Price Per Person",
      :address => "Address",
      :office_hours => "Office Hours",
      :tel => "Tel",
      :email => "Email",
      :website => "Website",
      :reccomment_menu => "Reccomment Menu",
      :map_img => "Map Img",
      :store_img => "Store Img",
      :restaurant_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Description/)
    rendered.should match(/Price Per Person/)
    rendered.should match(/Address/)
    rendered.should match(/Office Hours/)
    rendered.should match(/Tel/)
    rendered.should match(/Email/)
    rendered.should match(/Website/)
    rendered.should match(/Reccomment Menu/)
    rendered.should match(/Map Img/)
    rendered.should match(/Store Img/)
    rendered.should match(/1/)
  end
end
