require 'spec_helper'

describe "restaurants/new" do
  before(:each) do
    assign(:restaurant, stub_model(Restaurant,
      :description => "MyString",
      :price_per_person => "MyString",
      :address => "MyString",
      :office_hours => "MyString",
      :tel => "MyString",
      :email => "MyString",
      :website => "MyString",
      :reccomment_menu => "MyString",
      :map_img => "MyString",
      :store_img => "MyString",
      :restaurant_id => 1
    ).as_new_record)
  end

  it "renders new restaurant form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", restaurants_path, "post" do
      assert_select "input#restaurant_description[name=?]", "restaurant[description]"
      assert_select "input#restaurant_price_per_person[name=?]", "restaurant[price_per_person]"
      assert_select "input#restaurant_address[name=?]", "restaurant[address]"
      assert_select "input#restaurant_office_hours[name=?]", "restaurant[office_hours]"
      assert_select "input#restaurant_tel[name=?]", "restaurant[tel]"
      assert_select "input#restaurant_email[name=?]", "restaurant[email]"
      assert_select "input#restaurant_website[name=?]", "restaurant[website]"
      assert_select "input#restaurant_reccomment_menu[name=?]", "restaurant[reccomment_menu]"
      assert_select "input#restaurant_map_img[name=?]", "restaurant[map_img]"
      assert_select "input#restaurant_store_img[name=?]", "restaurant[store_img]"
      assert_select "input#restaurant_restaurant_id[name=?]", "restaurant[restaurant_id]"
    end
  end
end
