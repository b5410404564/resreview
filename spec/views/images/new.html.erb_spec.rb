require 'spec_helper'

describe "images/new" do
  before(:each) do
    assign(:image, stub_model(Image,
      :image => "MyString",
      :restaurant_id => 1
    ).as_new_record)
  end

  it "renders new image form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", images_path, "post" do
      assert_select "input#image_image[name=?]", "image[image]"
      assert_select "input#image_restaurant_id[name=?]", "image[restaurant_id]"
    end
  end
end
