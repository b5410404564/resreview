require 'spec_helper'

describe "users/show" do
  before(:each) do
    @user = assign(:user, stub_model(User,
      :name => "Name",
      :email => "Email",
      :phone => "Phone",
      :password_digest => "Password Digest",
      :avatar => "Avatar",
      :type => "Type"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Email/)
    rendered.should match(/Phone/)
    rendered.should match(/Password Digest/)
    rendered.should match(/Avatar/)
    rendered.should match(/Type/)
  end
end
