require 'spec_helper'

describe "users/index" do
  before(:each) do
    assign(:users, [
      stub_model(User,
        :name => "Name",
        :email => "Email",
        :phone => "Phone",
        :password_digest => "Password Digest",
        :avatar => "Avatar",
        :type => "Type"
      ),
      stub_model(User,
        :name => "Name",
        :email => "Email",
        :phone => "Phone",
        :password_digest => "Password Digest",
        :avatar => "Avatar",
        :type => "Type"
      )
    ])
  end

  it "renders a list of users" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Password Digest".to_s, :count => 2
    assert_select "tr>td", :text => "Avatar".to_s, :count => 2
    assert_select "tr>td", :text => "Type".to_s, :count => 2
  end
end
