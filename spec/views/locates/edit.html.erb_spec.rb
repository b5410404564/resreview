require 'spec_helper'

describe "locates/edit" do
  before(:each) do
    @locate = assign(:locate, stub_model(Locate,
      :area_name => "MyString"
    ))
  end

  it "renders the edit locate form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", locate_path(@locate), "post" do
      assert_select "input#locate_area_name[name=?]", "locate[area_name]"
    end
  end
end
