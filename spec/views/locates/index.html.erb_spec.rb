require 'spec_helper'

describe "locates/index" do
  before(:each) do
    assign(:locates, [
      stub_model(Locate,
        :area_name => "Area Name"
      ),
      stub_model(Locate,
        :area_name => "Area Name"
      )
    ])
  end

  it "renders a list of locates" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Area Name".to_s, :count => 2
  end
end
