require 'spec_helper'

describe "locates/show" do
  before(:each) do
    @locate = assign(:locate, stub_model(Locate,
      :area_name => "Area Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Area Name/)
  end
end
