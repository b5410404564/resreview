require 'spec_helper'

describe "locates/new" do
  before(:each) do
    assign(:locate, stub_model(Locate,
      :area_name => "MyString"
    ).as_new_record)
  end

  it "renders new locate form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", locates_path, "post" do
      assert_select "input#locate_area_name[name=?]", "locate[area_name]"
    end
  end
end
