require 'spec_helper'

describe "restaurant_categories/index" do
  before(:each) do
    assign(:restaurant_categories, [
      stub_model(RestaurantCategory,
        :name => "Name"
      ),
      stub_model(RestaurantCategory,
        :name => "Name"
      )
    ])
  end

  it "renders a list of restaurant_categories" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
