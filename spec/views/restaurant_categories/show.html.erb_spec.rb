require 'spec_helper'

describe "restaurant_categories/show" do
  before(:each) do
    @restaurant_category = assign(:restaurant_category, stub_model(RestaurantCategory,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
