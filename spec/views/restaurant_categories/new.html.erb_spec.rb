require 'spec_helper'

describe "restaurant_categories/new" do
  before(:each) do
    assign(:restaurant_category, stub_model(RestaurantCategory,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new restaurant_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", restaurant_categories_path, "post" do
      assert_select "input#restaurant_category_name[name=?]", "restaurant_category[name]"
    end
  end
end
