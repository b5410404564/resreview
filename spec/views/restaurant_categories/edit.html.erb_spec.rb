require 'spec_helper'

describe "restaurant_categories/edit" do
  before(:each) do
    @restaurant_category = assign(:restaurant_category, stub_model(RestaurantCategory,
      :name => "MyString"
    ))
  end

  it "renders the edit restaurant_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", restaurant_category_path(@restaurant_category), "post" do
      assert_select "input#restaurant_category_name[name=?]", "restaurant_category[name]"
    end
  end
end
