class AddDefaultTotalToRestaurant < ActiveRecord::Migration
  def change
        change_column :restaurants, :total, :float, :default => 0.0
  end
end