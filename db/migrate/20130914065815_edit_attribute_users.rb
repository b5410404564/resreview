class EditAttributeUsers < ActiveRecord::Migration
  def change
  	add_column :users, :admin, :boolean, :default => false
  	remove_column :users, :cat_user
  end
end
