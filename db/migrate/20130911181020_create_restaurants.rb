class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :address
      t.string :tel
      t.string :email
      t.string :website
      t.string :description
      t.string :price_per_person
      t.string :office_hours
      t.integer :locate_id
      t.integer :user_id
      t.integer :restaurant_category_id

      t.timestamps
    end
  end
end
