class AddDefaultValueRating < ActiveRecord::Migration
  def change
  	change_column :ratings, :total, :float, :default => 0.0
  end
end
