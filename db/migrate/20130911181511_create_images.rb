class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :image
      t.string :description
      t.integer :restaurant_id

      t.timestamps
    end
  end
end
