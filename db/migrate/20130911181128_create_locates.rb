class CreateLocates < ActiveRecord::Migration
  def change
    create_table :locates do |t|
      t.string :district

      t.timestamps
    end
  end
end
