class ChangeColumnAndAddRating < ActiveRecord::Migration
  def change
  	change_column :ratings, :total, :float
  	add_column :restaurants, :total, :float 
  end
end
