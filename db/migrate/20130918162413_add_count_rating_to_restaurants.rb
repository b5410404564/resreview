class AddCountRatingToRestaurants < ActiveRecord::Migration
  def change
    add_column :restaurants, :count, :integer,:default => 0
  end
end
