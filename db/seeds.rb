# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



RestaurantCategory.create([
	{name: 'Hotel Restaurant'},
	{name: "Pub&Restaurant"},
	{name: "Restaurant"},
	{name: "Street Food"},
	{name: "Buffet"}
	])

Locate.create([
	{district: 'Bang Bon(บางบอน)'},
	{district: 'Bang Kapi(บางกะปิ )'},
	{district: 'Bang Khae(บางแค)'},
	{district: 'Bang Khen(บางเขน)'},
	{district: 'Bang Kho Laem(บางคอแหลม)'},
	{district: 'Bang Khun Thian(บางขุนเทียน)'},
	{district: 'Bang Na(บางนา)'},
	{district: 'Bang Phlat(บางพลัด)'},
	{district: 'Bang Rak(บางรัก)'},
	{district: 'Bang Sue(บางซื่อ)'},
	{district: 'Bangkok Noi(บางกอกน้อย)'},
	{district: 'Bangkok Yai(บางกอกใหญ่)'},
	{district: 'Bueng Kum(บึงกุ่ม)'},
	{district: 'Chatuchak(จตุจักร)'},
	{district: 'Chom Thong(จอมทอง)'},
	{district: 'Din Daeng(ดินแดง)'},
	{district: 'Don Mueang(ดอนเมือง)'},
	{district: 'Dusit(ดุสิต)'},
	{district: 'Huai Khwang(ห้วยขวาง)'},
	{district: 'Khan Na Yao(คันนายาว)'},
	{district: 'Khlong Sam Wa(คลองสามวา)'},
	{district: 'Khlong San(คลองสาน)'},
	{district: 'Khlong Toei(คลองเตย)'},
	{district: 'Lak Si(หลักสี่ )'},
	{district: 'Lat Krabang(ลาดกระบัง)'},
	{district: 'Lat Phrao(ลาดพร้าว)'},
	{district: 'Min Buri(มีนบุรี )'},
	{district: 'Nong Chok(หนองจอก)'},
	{district: 'Nong Khaem(หนองแขม)'},
	{district: 'Pathum Wan(ปทุมวัน)'},
	{district: 'Phasi Charoen(ภาษีเจริญ)'},
	{district: 'Phaya Thai(พญาไท)'},
	{district: 'Phra Khanong(พระโขนง)'},
	{district: 'Phra Nakhon(พระนคร)'},
	{district: 'Pom Prap Sattru Phai(ป้อมปราบศัตรูพ่าย)'},
	{district: 'Prawet(ประเวศ)'},
	{district: 'Rat Burana(ราษฎร์บูรณะ)'},
	{district: 'Ratchathewi(ราชเทวี )'},
	{district: 'Sai Mai(สายไหม)'},
	{district: 'Samphanthawong(สัมพันธวงศ์)'},
	{district: 'Saphan Sung(สะพานสูง)'},
	{district: 'Sathon(สาทร)'},
	{district: 'Suan Luang(สวนหลวง)'},
	{district: 'Taling Chan(ตลิ่งชัน)'},
	{district: 'Thawi Watthana(ทวีวัฒนา)'},
	{district: 'Thon Buri District(ธนบุรี )'},
	{district: 'Thung Khru(ทุ่งครุ)'},
	{district: 'Wang Thonglang(วังทองหลาง)'},
	{district: 'Watthana(วัฒนา)'},
	{district: 'Yan Nawa(ยานนาวา)'}
	])

